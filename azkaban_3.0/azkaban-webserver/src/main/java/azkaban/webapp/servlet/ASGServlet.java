package azkaban.webapp.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import azkaban.asgHandler.ASGRequest;
import azkaban.asgHandler.ASGResponse;
import azkaban.asgHandler.IPWithStatus;
import azkaban.executor.ExecutorManagerException;
import azkaban.project.JdbcProjectLoader;
import azkaban.project.ProjectManagerException;
import azkaban.server.session.Session;
import azkaban.utils.AwsASGUtils;
import azkaban.utils.AwsEC2Utils;
import azkaban.utils.Props;
import azkaban.utils.SleepUtils;
import azkaban.utils.ValidateEC2Service;
import azkaban.utils.retry.RetryChecker;
import azkaban.utils.retry.RetryOperation;
import azkaban.webapp.AzkabanWebServer;

public class ASGServlet extends LoginAbstractAzkabanServlet {

	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = Logger.getLogger(ASGServlet.class.getName());
	private AwsASGUtils asgUtil = new AwsASGUtils();
	private AwsEC2Utils ec2Util = new AwsEC2Utils();
	private ValidateEC2Service ec2Service = new ValidateEC2Service();
	private int MAX_RETRY = 3;
	private int WAIT_TIME_IN_SECONDS = 5;
	private int TERMINATE_WAIT_TIME_IN_SECONDS = 120;
	private AzkabanWebServer server;
	private Props props;
	private JdbcProjectLoader projectLoader;

	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		this.server = (AzkabanWebServer) getApplication();
		this.props = this.server.getServerProps();
		this.projectLoader = new JdbcProjectLoader(props);
	}

	@Override
	protected void handleGet(HttpServletRequest httpRequest, HttpServletResponse httpResponse, Session session)
			throws ServletException, IOException {
		ASGResponse asgResponse = new ASGResponse();

		if (hasParam(httpRequest, ServletConstants.ACTION)) {
			String action = getParam(httpRequest, ServletConstants.ACTION);
			if (action.equals(ServletConstants.CHECK_INSTANCES)) {
				asgResponse = getInstancesStates(httpRequest, session);
			} else {
				asgResponse = getErrorResponse(ServletConstants.INVALID, ServletConstants.ErrorMessage.INVALID_REQUEST,
						400);
			}
		} else if (hasParam(httpRequest, ServletConstants.ASG_NAME)) {
			String asgName = getValueFromParam(httpRequest, ServletConstants.ASG_NAME, null);
			asgResponse = getASGDetailsWithRetry(asgName);
		} else {
			asgResponse = getErrorResponse(ServletConstants.UNKNOWN, ServletConstants.ErrorMessage.UNSUPPORTED_API,
					404);
		}
		sendResponse(httpResponse, asgResponse.withApi(ServletConstants.STATUS));
	}

	private ASGResponse getErrorResponse(String apiName, String errMessage, int statusCode) {
		return new ASGResponse().withApi(apiName).withErrMessage(errMessage).withStatusCode(statusCode);
	}

	@Override
	protected void handlePost(HttpServletRequest httpRequest, HttpServletResponse httpResponse, Session session)
			throws ServletException, IOException {
		ASGResponse asgResponse = new ASGResponse();

		if (hasParam(httpRequest, ServletConstants.ACTION)) {
			String action = getParam(httpRequest, ServletConstants.ACTION);
			ASGRequest asgRequest = getAsgRequestFromUserParams(httpRequest, session);
			if (action.equals(ServletConstants.INCREASE)) {
				asgResponse = increaseASGWithRetry(asgRequest);
			} else if (action.equals(ServletConstants.DECREASE)) {
				asgResponse = decreaseASGWithRetry(asgRequest);
			} else {
				asgResponse = getErrorResponse(ServletConstants.ACTION, ServletConstants.ErrorMessage.INVALID_REQUEST,
						400);
			}

		} else {
			asgResponse = getErrorResponse(ServletConstants.ACTION, ServletConstants.ErrorMessage.UNSUPPORTED_API, 404);
		}
		sendResponse(httpResponse, asgResponse);
	}

	private ASGRequest getAsgRequestFromUserParams(HttpServletRequest req, Session session) throws ServletException {
		String asgName = getValueFromParam(req, ServletConstants.ASG_NAME, null);

		int minSize = getValueFromParam(req, ServletConstants.MIN_SIZE);
		int maxSize = getValueFromParam(req, ServletConstants.MAX_SIZE);
		int desiredCapacity = getValueFromParam(req, ServletConstants.DESIRED_CAPACITY);

		ASGRequest asgRequest = new ASGRequest(asgName, minSize, desiredCapacity, maxSize);

		LOGGER.info("ASG Request : " + asgRequest);
		return asgRequest;
	}

	private int getValueFromParam(HttpServletRequest req, String key) throws ServletException {
		return hasParam(req, key) ? getIntParam(req, key) : 0;
	}

	private String getValueFromParam(HttpServletRequest req, String key, String defaultValue) throws ServletException {
		return hasParam(req, key) ? getParam(req, key) : defaultValue;
	}

	private ASGResponse increaseASGWithRetry(ASGRequest asgRequest) {
		ASGResponse asgResponse = new ASGResponse();
		String asgName = asgRequest.getAsgName();
		RetryOperation retry = new RetryOperation(MAX_RETRY, WAIT_TIME_IN_SECONDS, TimeUnit.SECONDS);
		try {
			asgResponse = retry.executeWithRetry(new Callable<ASGResponse>() {

				@Override
				public ASGResponse call() throws Exception {
					asgUtil.updateRequest(asgRequest);
					return new ASGResponse(asgName).withAsgDetails(asgUtil.describeASGroupsAsString(asgName))
							.withStatusCode(200);
				}
			}, getRetryer());
		} catch (Exception e) {
			asgResponse = new ASGResponse(asgName)
					.withErrMessage(ServletConstants.ErrorMessage.UNABLE_TO_PROCESS_REQUEST + e.getMessage())
					.withStatusCode(400);
		}
		return asgResponse.withApi(ServletConstants.UPDATE).withAsgName(asgName);
	}

	private ASGResponse getASGDetailsWithRetry(String asgName) {
		RetryOperation retry = new RetryOperation(MAX_RETRY, WAIT_TIME_IN_SECONDS, TimeUnit.SECONDS);
		ASGResponse asgResponse = new ASGResponse();
		try {
			asgResponse = retry.executeWithRetry(new Callable<ASGResponse>() {

				@Override
				public ASGResponse call() throws Exception {
					return new ASGResponse(asgName).withAsgDetails(asgUtil.describeASGroupsAsString(asgName))
							.withStatusCode(200);
				}
			}, getRetryer());
		} catch (Exception e) {
			asgResponse.withErrMessage(e.getMessage()).withStatusCode(400);
		}
		return asgResponse.withApi(ServletConstants.STATUS).withAsgName(asgName);
	}

	private ASGResponse getInstancesStates(HttpServletRequest httpRequest, Session session) throws ServletException {
		String asgName = getValueFromParam(httpRequest, ServletConstants.ASG_NAME, null);
		RetryOperation retry = new RetryOperation(1, 0, TimeUnit.SECONDS);
		try {
			return retry.executeWithRetry(new Callable<ASGResponse>() {

				@Override
				public ASGResponse call() throws Exception {

					List<String> instanceIds = asgUtil.getInstanceIds(asgName);
					List<IPWithStatus> ips = ec2Util.getIpAddresses(instanceIds);
					if (!ec2Service.validateServicesOnIps(ips)) {
						return new ASGResponse(asgName).withIps(instanceIds).withStatusCode(400)
								.withErrMessage("Services is not running atleast one or more machine")
								.withIPWithStatus(ips);
					}

					return new ASGResponse(asgName).withIps(instanceIds).withStatusCode(200).withIPWithStatus(ips);
				}
			}, getRetryer());
		} catch (Exception e) {
			return new ASGResponse(asgName).withErrMessage(e.getMessage()).withStatusCode(400);
		}
	}

	private RetryChecker getRetryer() {
		// TODO : Handle separately Throttling Exception and Service Exceptions
		return new RetryChecker() {
			@Override
			public boolean shouldRetry(Exception ex) {
				if (ex instanceof Exception) {
					LOGGER.info("Retrying on Exception : {}" + ex.getMessage());
					return true;
				}
				return false;
			}
		};
	}

	private void sendResponse(HttpServletResponse response, ASGResponse asgresponse) throws IOException {
		LOGGER.info("ASG  Response : " + asgresponse);
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		PrintWriter out = response.getWriter();
		out.print(asgresponse.toString());
		out.flush();
	}

	protected List<IPWithStatus> getAllIPStatus(String asgName) throws Exception {
		List<String> instanceIds = asgUtil.getInstanceIds(asgName);
		return ec2Util.getIpAddresses(instanceIds);
	}

	private ASGResponse decreaseASGWithRetry(ASGRequest asgRequest) {
		ASGResponse asgResponse = new ASGResponse();
		String asgName = asgRequest.getAsgName();
		RetryOperation retry = new RetryOperation(MAX_RETRY, WAIT_TIME_IN_SECONDS, TimeUnit.SECONDS);
		try {
			asgResponse = retry.executeWithRetry(new Callable<ASGResponse>() {
				@Override
				public ASGResponse call() throws Exception {
					asgUtil.updateRequest(asgRequest);
					LOGGER.info("Sleeping for [" + TERMINATE_WAIT_TIME_IN_SECONDS + "] seconds");
					SleepUtils.sleep(TERMINATE_WAIT_TIME_IN_SECONDS);
					updateExectorTable(asgName);
					return new ASGResponse(asgName).withAsgDetails(asgUtil.describeASGroupsAsString(asgName))
							.withStatusCode(200);
				}
			}, getRetryer());
		} catch (Exception e) {
			asgResponse = new ASGResponse(asgName)
					.withErrMessage(ServletConstants.ErrorMessage.UNABLE_TO_PROCESS_REQUEST + e.getMessage())
					.withStatusCode(400);
		}
		return asgResponse.withApi(ServletConstants.UPDATE).withAsgName(asgName);
	}

	protected void updateExectorTable(String asgName) throws Exception {
		List<String> activeInstances = asgUtil.getInstanceIds(asgName, true);
		List<IPWithStatus> ipStatus = ec2Util.getIpAddresses(activeInstances);
		List<String> activeIps = new ArrayList<>();

		String tag = asgUtil.getASGExecTag(asgName);
		String query = "delete from executors  where executor_tag=\"" + tag + "\"";

		if (activeInstances.size() > 0) {
			LOGGER.info("Decrease ASG request issued, still active instances exist, waiting for 2 minutes");
			SleepUtils.sleep(TERMINATE_WAIT_TIME_IN_SECONDS);
			//Get active instnaces (InService), if there are any exclude from deletion.
			activeInstances = asgUtil.getInstanceIds(asgName,true);
			if(activeInstances.size()>0) {
				LOGGER.info("Found few In-active Executors, so updating Executor table" + ipStatus);
				for (IPWithStatus ip : ipStatus) {
					activeIps.add("\"" + ip.getIp() + "\"");
				}
				String ipsWithComma = org.apache.commons.lang.StringUtils.join(activeIps, ",");
				query += " and host not in (" + ipsWithComma + ")";
			}

		} else {
			LOGGER.info("No Inactive Ips Found, So Nothing to update");
		}
		updateAndReloadExecutors(query);
	}

	private void updateAndReloadExecutors(String query) throws Exception {
		LOGGER.info("Final Query : " + query);
		try {
			projectLoader.updateExecutorTable(query);
			server.getExecutorManager().setupExecutors();
		} catch (ExecutorManagerException e) {
			throw e;
		} catch (ProjectManagerException e) {
			throw e;
		} catch (Exception e) {
			throw e;
		}
	}
}
