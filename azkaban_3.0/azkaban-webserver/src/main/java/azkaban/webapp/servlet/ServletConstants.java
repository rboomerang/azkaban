package azkaban.webapp.servlet;

public class ServletConstants {
	public static final String SUCCESS = "SUCCESS";
	public static final String FAILED = "FAILED";
	public static final String ASG_NAME = "asgName";
	public static final String CHECK_INSTANCES = "checkInstances";
	public static final String UNKNOWN = "UNKNOWN";
	public static final String INVALID = "Invalid Request";
	public static final String ACTION = "action";
	public static final String UPDATE = "updateAsg";
	public static final String GET_IPS = "getIps";
	public static final String MIN_SIZE = "minSize";
	public static final String MAX_SIZE = "maxSize";
	public static final String DESIRED_CAPACITY = "desiredCapacity";
	public static final String STATUS = "status";
	public static final String INCREASE = "increaseAsg";
	public static final String DECREASE = "decreaseAsg";
	public static final String HEALTHY = "Healthy";

	class ErrorMessage {
		public static final String UNSUPPORTED_API = "Unsupported API got hit. ";
		public static final String INVALID_REQUEST = "Invalid Request has been passed. ";
		public static final String UNABLE_TO_PROCESS_REQUEST = "Unable to process ASG update Request. ";
	}

}
