create database if not exists azkaban;
delete from mysql.user where User='azkaban';
CREATE USER 'azkaban'@'%' IDENTIFIED BY 'azkaban';
GRANT CREATE,ALTER,SELECT,INSERT,UPDATE,DELETE,INDEX ON azkaban.* to 'azkaban'@'%' WITH GRANT OPTION;
CREATE USER 'azkaban'@'localhost' IDENTIFIED BY 'azkaban';
GRANT CREATE,ALTER,SELECT,INSERT,UPDATE,DELETE,INDEX ON azkaban.* to 'azkaban'@'localhost' WITH GRANT OPTION;