create table if not exists executor_tag_info(
name varchar(64) not NULL ,
email VARCHAR(100) DEFAULT "notification@email.com",
active boolean DEFAULT true,
PRIMARY KEY(NAME)
);