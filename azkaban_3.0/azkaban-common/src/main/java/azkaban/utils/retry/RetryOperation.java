package azkaban.utils.retry;

import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

/**
 * @author allwin
 *
 */
public class RetryOperation {
	private static final Logger LOGGER = Logger.getLogger(RetryOperation.class.getName());

	private int maxRetry;

	private long delay;

	private TimeUnit unit;

	private int retryCount;

	public RetryOperation(int maxRetry, long delay, TimeUnit unit) {
		this.maxRetry = maxRetry;
		this.delay = delay;
		this.unit = unit;
		this.retryCount = 0;
	}

	public <T> T executeWithRetry(Callable<T> callable, RetryChecker checker) throws Exception {

		try {
			LOGGER.info("Retry Count : " + retryCount);
			return callable.call();
		} catch (Exception e) {
			if (checker.shouldRetry(e) && retryCount < maxRetry) {
				retryCount++;
				unit.sleep(delay);
				return executeWithRetry(callable, checker);
			} else {
				throw e;
			}
		}
	}
}
