package azkaban.utils.retry;

public interface RetryChecker {

	boolean shouldRetry(Exception ex);
}