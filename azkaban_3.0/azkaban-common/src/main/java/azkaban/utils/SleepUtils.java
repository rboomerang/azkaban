package azkaban.utils;

import org.apache.log4j.Logger;

public class SleepUtils {
	private static final Logger LOGGER = Logger.getLogger(SleepUtils.class.getName());

	public static void sleep(int seconds) {
		try {
			Thread.sleep(seconds * 1000);
		} catch (Exception e) {
			LOGGER.info("Retrying on Exception : {}" + e.getMessage());
		}
	}
}