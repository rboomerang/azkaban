package azkaban.utils;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;

import com.amazonaws.auth.InstanceProfileCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.autoscaling.AmazonAutoScaling;
import com.amazonaws.services.autoscaling.AmazonAutoScalingClientBuilder;
import com.amazonaws.services.autoscaling.model.AutoScalingGroup;
import com.amazonaws.services.autoscaling.model.DescribeAutoScalingGroupsRequest;
import com.amazonaws.services.autoscaling.model.DescribeAutoScalingGroupsResult;
import com.amazonaws.services.autoscaling.model.DescribeTagsRequest;
import com.amazonaws.services.autoscaling.model.DescribeTagsResult;
import com.amazonaws.services.autoscaling.model.Filter;
import com.amazonaws.services.autoscaling.model.Instance;
import com.amazonaws.services.autoscaling.model.TagDescription;
import com.amazonaws.services.autoscaling.model.UpdateAutoScalingGroupRequest;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;

import azkaban.asgHandler.ASGRequest;

public class AwsASGUtils {

	private static final Logger logger = Logger.getLogger(AwsASGUtils.class.getName());
	private final AmazonAutoScaling asg;
	private final AmazonEC2 ec2;

	public AwsASGUtils() {
		this.asg = AmazonAutoScalingClientBuilder.standard().withRegion(Regions.US_WEST_2)
				.withCredentials(new InstanceProfileCredentialsProvider(false)).build();
		this.ec2 = AmazonEC2ClientBuilder.standard().withRegion(Regions.US_WEST_2)
				.withCredentials(new InstanceProfileCredentialsProvider(false)).build();
	}

	public List<AutoScalingGroup> listASGroups() {
		logger.info("list all auto scaling groups");
		List<AutoScalingGroup> asGroups = new ArrayList<>();
		String nextToken = null;
		while (true) {
			DescribeAutoScalingGroupsResult result = asg
					.describeAutoScalingGroups(new DescribeAutoScalingGroupsRequest().withNextToken(nextToken));
			asGroups.addAll(result.getAutoScalingGroups());
			nextToken = result.getNextToken();
			if (nextToken == null)
				break;
		}
		return asGroups;
	}

	public AutoScalingGroup describeASGroups(String asGroupName) {
		logger.info("describe auto scaling group, name= " + asGroupName);
		List<AutoScalingGroup> groups = asg
				.describeAutoScalingGroups(
						new DescribeAutoScalingGroupsRequest().withAutoScalingGroupNames(asGroupName))
				.getAutoScalingGroups();
		if (groups.isEmpty())
			return null;
		logger.info("Retrieved ASG Details : " + groups.get(0));
		return groups.get(0);
	}

	public AutoScalingGroup checkServices(String asGroupName) {
		logger.info("describe auto scaling group, name= " + asGroupName);
		List<AutoScalingGroup> groups = asg
				.describeAutoScalingGroups(
						new DescribeAutoScalingGroupsRequest().withAutoScalingGroupNames(asGroupName))
				.getAutoScalingGroups();
		if (groups.isEmpty())
			return null;

		logger.info("Retrieved ASG Details : " + groups.get(0));
		return groups.get(0);
	}

	public String describeASGroupsAsString(String asGroupName) {
		return describeASGroups(asGroupName).toString();
	}

	public String getASGExecTag(String asGroupName) {
		AutoScalingGroup asg1 = describeASGroups(asGroupName);
		for (TagDescription tag : asg1.getTags()) {
			logger.info("Tag : " + tag);
			if (tag.getKey().equalsIgnoreCase("executor_tag")) {
				return tag.getValue();
			}
		}
		return null;
	}

	public void updateRequest(ASGRequest asgRequest) {
		asg.updateAutoScalingGroup(new UpdateAutoScalingGroupRequest().withAutoScalingGroupName(asgRequest.getAsgName())
				.withDesiredCapacity(asgRequest.getDesiredCapacity()).withMaxSize(asgRequest.getMaxSize())
				.withMinSize(asgRequest.getMinSize()));
		logger.info("Successfully updated the Request : " + asgRequest);
	}

	public List<String> getInstanceIds(String asgGroupName) throws Exception {
		logger.info("Get All Public Ips of Asg " + asgGroupName);
		List<String> instanceIds = new LinkedList<String>();

		AutoScalingGroup asg = describeASGroups(asgGroupName);
		if (null == asg) {
			throw new Exception("No Such ASG Name found : " + asgGroupName);
		}
		logger.info("Number of Instances found for the ASG is : " + asg.getInstances().size());
		if (asg.getInstances().size() != 0) {
			for (Instance instance : asg.getInstances()) {
				logger.info("Found Instace Id : " + instance);
				instanceIds.add(instance.getInstanceId());
			}
		}

		return instanceIds;
	}

	public List<String> getInstanceIds(String asgGroupName, boolean isOnlyActiveMachices) {
		logger.info("Get All Public Ips of Asg " + asgGroupName);
		AutoScalingGroup asg = describeASGroups(asgGroupName);
		List<String> activeInstances = new ArrayList<>();
		for (Instance instance : asg.getInstances()) {
			if (isOnlyActiveMachices) {
				if ("InService".equalsIgnoreCase(instance.getLifecycleState())) {
					logger.info("Found an Instance with InService");
					activeInstances.add(instance.getInstanceId());
				}
			} else {
				activeInstances.add(instance.getInstanceId());
			}
		}
		return activeInstances;
	}

	public String getExecutorTag(AutoScalingGroup asg) {
		String asgTag = null;
		if (asg == null) {
			return asgTag;
		}
		List<TagDescription> asgTags = asg.getTags();
		for (TagDescription tagDes : asgTags) {
			if (tagDes.getKey().equals("executor_tag"))
				asgTag = tagDes.getValue();
		}
		return asgTag;
	}
	/*
	 * public String getExecutorTag(String asgName) { AutoScalingGroup abc =
	 * checkServices(asgName); return getExecutorTag(abc); }
	 */

	public void increaseRequest(ASGRequest asgRequest) {
		updateRequest(asgRequest);
		logger.info("Successfully updated the increase Request : " + asgRequest);
	}
}
