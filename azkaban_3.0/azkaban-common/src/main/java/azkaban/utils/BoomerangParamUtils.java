package azkaban.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class BoomerangParamUtils {

	/**
	 * Checks if a given key is a variable property<br>
	 * e.g. true for ${username}
	 */
	public static boolean isVariable(String key) {
		return key.matches("^\\$\\{[^{}]+\\}$");
	}

	/**
	 * Extracts the key name from the given variable.<br>
	 * e.g. ${username} to username
	 */
	public static String getVariableName(String key) {
		if (isVariable(key)) {
			return key.substring(2, key.length() - 1);
		} else {
			return key;
		}
	}

	/**
	 * Resolves the key w.r.t the given Properties, if the key is a variable
	 * e.g. ${username} to "boomerang-commerce"
	 */
	public static String resolveIfVariable(String key, Map<String, String> map) {
		if (key == null || key.isEmpty()) {
			return key;
		} else if (isVariable(key)) {
			return map.get(getVariableName(key));
		} else {
			return key;
		}
	}

	/**
	 * Splits the given csv on commas and trims the resulting items
	 */
	public static List<String> splitAndTrim(String csv) {
		List<String> values = new ArrayList<String>();
		if (csv != null) {
			for (String value : csv.split(",")) {
				value = value.trim();
				if (!value.isEmpty()) {
					values.add(value);
				}
			}
		}

		return values;
	}

}
