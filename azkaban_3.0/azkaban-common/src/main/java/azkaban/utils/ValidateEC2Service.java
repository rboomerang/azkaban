package azkaban.utils;

import java.io.IOException;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.log4j.Logger;

import azkaban.asgHandler.IPWithStatus;

public class ValidateEC2Service {

	private static final Logger LOG = Logger.getLogger(ValidateEC2Service.class.getName());
	private static final String HEALTH_CHECK_ENDPOINT = "healthCheck";
	private static final String HEALTH_CHECK_PORT = "12321";
	private static final String IP_FORMAT = "http://%s:%s/%s";
	private static final String DEFAULT_OUTPUT = "{\"status\" : \"success\"}";

	public boolean isServiceRunning(String ip) throws ClientProtocolException, IOException {
		String url = String.format(IP_FORMAT, ip, HEALTH_CHECK_PORT, HEALTH_CHECK_ENDPOINT);
		return isServiceRunning(url, DEFAULT_OUTPUT);

	}

	public boolean isServiceRunning(String url, String expectedOutput) throws ClientProtocolException, IOException {

		boolean isServiceRunning = false;
		LOG.info("URL : " + url + "\t expectedOutput : " + expectedOutput);
		try {
			execute(url);
			isServiceRunning = true;
			LOG.info("Service is running at URL : " + url);
		} catch (Exception e) {
			LOG.error("Exception occured :" + e.getMessage());
			throw e;
		}
		return isServiceRunning;
	}

	private void execute(String url) throws ClientProtocolException, IOException {
		HttpClient client = HttpClientBuilder.create().build();
		HttpGet request = new HttpGet(url);
		HttpResponse response = client.execute(request);
		LOG.info("Service Check Response is : " + response);
	}

	public boolean validateServicesOnIps(List<IPWithStatus> ips) {
		boolean isAllIpsRunningWithServices = true;
		for (IPWithStatus ip : ips) {
			try {
				LOG.info("Checking service is running on IP : " + ip.getIp());
				isServiceRunning(ip.getIp());
			} catch (Exception e) {
				LOG.error("Error while checking service is running on IP : " + ip.getIp() + " : " + e.getMessage());
				isAllIpsRunningWithServices = false;
				ip.setStatusCode(400);
				ip.setStatusMessage(e.getMessage());
			}
		}
		return isAllIpsRunningWithServices;
	}

}
