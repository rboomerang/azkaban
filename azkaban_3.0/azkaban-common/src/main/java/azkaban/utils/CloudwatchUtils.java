package azkaban.utils;

import azkaban.executor.Executor;
import azkaban.executor.ExecutorInfo;
import azkaban.executor.ExecutorManager;
import com.amazonaws.auth.*;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.cloudwatch.AmazonCloudWatch;
import com.amazonaws.services.cloudwatch.AmazonCloudWatchClientBuilder;
import com.amazonaws.services.cloudwatch.model.*;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by akshay on 20/03/18.
 */
public class CloudwatchUtils {
    private static Logger LOG = Logger.getLogger(ExecutorManager.class);

    private static final String CPU="cpu";
    private static final String MEMORY="memory";
    private static final String REMAINING_FLOWS="remainingflows";

    final private AmazonCloudWatch cw;
    Props azkProps;
    public CloudwatchUtils(Props azkProps, boolean isTestMode) {
        if(isTestMode){
            BasicAWSCredentials basicAWSCreds= (BasicAWSCredentials) getSessionCreds();
            this.cw = AmazonCloudWatchClientBuilder.standard().withRegion(Regions.US_WEST_2).withCredentials(new AWSStaticCredentialsProvider(basicAWSCreds)).build();
        }else{
            this.cw = AmazonCloudWatchClientBuilder.standard().withRegion(Regions.US_WEST_2).withCredentials(new InstanceProfileCredentialsProvider(false)).build();

        }
        this.azkProps=azkProps;
    }

    private AWSCredentials getSessionCreds(){
        AWSCredentials awsCredentials = null;
        return awsCredentials;
    }
    public void publishMetrics(Map<String, Executor> executorInfo){
        LOG.info("Inside publish metrics method");
        List<MetricDatum> metrics=new ArrayList<MetricDatum>();

        for(Map.Entry<String,Executor> executor: executorInfo.entrySet()){
            Dimension dimensionHost = new Dimension().withName("host").withValue(executor.getValue().getHost());


            MetricDatum cpu = getMetric(CPU, String.valueOf(executor.getValue().getExecutorInfo().getCpuUsage()), StandardUnit.None,dimensionHost);
            MetricDatum memory = getMetric(MEMORY, String.valueOf(executor.getValue().getExecutorInfo().getRemainingMemoryPercent()),StandardUnit.Percent,dimensionHost);
            MetricDatum remainingFlowsCapacity = getMetric(REMAINING_FLOWS, String.valueOf(executor.getValue().getExecutorInfo().getRemainingFlowCapacity()),StandardUnit.Count,dimensionHost);

            if(metrics.size()<=15){
                metrics.add(cpu);
                metrics.add(memory);
                metrics.add(remainingFlowsCapacity);
            }
            if(metrics.size()>15){
                sendMetrics(metrics);
                metrics.clear();
                metrics.add(cpu);
                metrics.add(memory);
                metrics.add(remainingFlowsCapacity);
            }
        }
        if(!metrics.isEmpty()){
            sendMetrics(metrics);
            metrics.clear();
        }
    }

    public void sendMetrics(List<MetricDatum> metrics){
        LOG.info("Metrics collection size"+metrics.size());
        PutMetricDataRequest request = new PutMetricDataRequest()
                .withNamespace(String.format("azkaban_monitoring_%s",azkProps.getString("azkaban.name").toLowerCase()))
                .withMetricData(metrics);
        PutMetricDataResult response = cw.putMetricData(request);
        LOG.info("Successfully pushed metrics, Result: "+response.getSdkResponseMetadata());
    }

    private MetricDatum getMetric(String metricName,String metricValue,StandardUnit unit,Dimension...dimension){
        MetricDatum datum = new MetricDatum()
                .withMetricName(metricName)
                .withUnit(unit)
                .withValue(Double.valueOf(metricValue))
                .withDimensions(dimension);
        return datum;
    }
}
