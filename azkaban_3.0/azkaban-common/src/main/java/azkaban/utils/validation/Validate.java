package azkaban.utils.validation;

public interface Validate {

	public <T> T validateWithResponse() throws Exception;

}
