package azkaban.utils;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

/**
 * @author allwin
 *
 */
public class RetryUtil {
	private static final Logger LOGGER = Logger.getLogger(RetryUtil.class.getName());
	private int numOfRetriesLeft;
	private int sleepSeconds;
	private boolean shouldRetry;
	private boolean isFirstTime;

	public RetryUtil(int numOfRetries, int sleepSeconds) {
		this.numOfRetriesLeft = numOfRetries;
		this.sleepSeconds = sleepSeconds;
		this.shouldRetry = true;
		this.isFirstTime = true;
	}

	public boolean canRetry() {
		if (this.shouldRetry && this.numOfRetriesLeft > 0) {
			if (!this.isFirstTime) {
				LOGGER.info("Retrying...");
			}

			this.isFirstTime = false;
			return true;
		} else if (this.shouldRetry && this.numOfRetriesLeft <= 0) {
			LOGGER.info("Number of retries exceeded...");
			return false;
		} else {
			return false;
		}
	}

	public void stopRetry() {
		this.numOfRetriesLeft = 0;
		this.shouldRetry = false;
	}

	public void errorOccured() {
		LOGGER.info("Sleep for " + (this.sleepSeconds / 60) + " mins before retrying..Retries left: "
				+ (this.numOfRetriesLeft - 1));

		try {
			TimeUnit.SECONDS.sleep((long) this.sleepSeconds);
		} catch (InterruptedException var2) {
			LOGGER.error("Interrupted during sleep..");
		}

		--this.numOfRetriesLeft;
	}

	public int getNumOfRetriesLeft() {
		return numOfRetriesLeft;
	}

	public void setNumOfRetriesLeft(int numOfRetriesLeft) {
		this.numOfRetriesLeft = numOfRetriesLeft;
	}

}
