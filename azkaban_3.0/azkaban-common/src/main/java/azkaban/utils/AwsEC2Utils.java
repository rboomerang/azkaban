package azkaban.utils;

import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;

import com.amazonaws.auth.InstanceProfileCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;
import com.amazonaws.services.ec2.model.DescribeInstancesRequest;
import com.amazonaws.services.ec2.model.DescribeInstancesResult;
import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.ec2.model.Reservation;
import com.amazonaws.services.ec2.model.Tag;

import azkaban.asgHandler.IPWithStatus;

public class AwsEC2Utils {

	private static final Logger LOG = Logger.getLogger(AwsEC2Utils.class.getName());
	private final AmazonEC2 ec2;

	public AwsEC2Utils() {
		this.ec2 = AmazonEC2ClientBuilder.standard().withRegion(Regions.US_WEST_2)
				.withCredentials(new InstanceProfileCredentialsProvider(false)).build();
	}

	public List<IPWithStatus> getIpAddresses(List<String> instanceIds) {
		List<IPWithStatus> ipWithStatus = new LinkedList<>();
		if (null == instanceIds || 0 == instanceIds.size()) {
			return ipWithStatus;
		}
		List<Reservation> reservations = getReservation(instanceIds);

		for (Reservation res : reservations) {
			for (Instance instance : res.getInstances()) {
				LOG.info("Private IP from Instance " + instance.getInstanceId() + " is "
						+ instance.getPrivateIpAddress());
				ipWithStatus.add(new IPWithStatus(instance.getInstanceId(), instance.getPrivateIpAddress()));
			}
		}
		return ipWithStatus;
	}

	public String getExecutorTag(List<String> instanceIds) {

		for (Reservation res : getReservation(instanceIds)) {
			for (Instance instance : res.getInstances()) {
				for (Tag tag : instance.getTags()) {
					if (tag.getKey().equals("executor_tag")) {
						LOG.info("Instance Id : [" + instance.getInstanceId() + "] Found with executor_tag "
								+ tag.getValue());
						return tag.getValue();
					}
				}
				LOG.info("Couldn't Find executor_tag for instance ID : " + instance.getInstanceId());
			}
		}

		return null;
	}

	private List<Reservation> getReservation(List<String> instanceIds) {
		DescribeInstancesRequest request = new DescribeInstancesRequest();
		request.setInstanceIds(instanceIds);

		DescribeInstancesResult result = ec2.describeInstances(request);
		return result.getReservations();
	}
}
