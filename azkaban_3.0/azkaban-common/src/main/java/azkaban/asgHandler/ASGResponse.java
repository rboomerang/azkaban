package azkaban.asgHandler;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import azkaban.utils.JSONUtils;

public class ASGResponse {

	private static final Logger LOGGER = Logger.getLogger(ASGResponse.class.getName());
	private int statusCode;
	private String api;
	private String asgName;
	private String asgDetails;
	private String errMessage;
	private List<String> ips;
	private List<IPWithStatus> ipWithStatus;

	public ASGResponse() {
		this.ips = new ArrayList<>();
		this.ipWithStatus = new ArrayList<>();
		this.statusCode = 200;
		this.errMessage = null;
	}

	public ASGResponse(String asgName) {
		this();
		this.asgName = asgName;
	}

	public ASGResponse withAsgName(String asgName) {
		this.asgName = asgName;
		return this;
	}

	public ASGResponse withApi(String api) {
		this.api = api;
		return this;
	}

	public String getStatus() {
		return this.statusCode == 200 ? "SUCCESS" : "FAILED";
	}

	public ASGResponse withAsgDetails(String asgDetails) {
		this.asgDetails = asgDetails;
		return this;
	}

	public ASGResponse withStatusCode(int statusCode) {
		this.statusCode = statusCode;
		return this;
	}

	public ASGResponse withErrMessage(String errMessage) {
		this.errMessage = errMessage;
		return this;
	}

	public ASGResponse withIps(List<String> ips) {
		this.ips = ips;
		return this;
	}

	public ASGResponse withIPWithStatus(List<IPWithStatus> ipWithStatus) {
		this.ipWithStatus = ipWithStatus;
		return this;
	}

	public int getStatusCode() {
		return this.statusCode;
	}

	public String getErrMessage() {
		return this.errMessage;
	}

	public List<String> getIps() {
		return this.ips;
	}

	public String getApi() {
		return api;
	}

	public String getAsgName() {
		return asgName;
	}

	public String getAsgDetails() {
		return asgDetails;
	}

	public List<IPWithStatus> getIpWithStatus() {
		return ipWithStatus;
	}

	@Override
	public String toString() {
		return JSONUtils.toJSON(this);
	}

}
