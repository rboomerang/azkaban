package azkaban.asgHandler;

import azkaban.utils.JSONUtils;

public class ASGRequest {

	private String asgName;

	private int minSize;
	private int desiredCapacity;
	private int maxSize;

	public ASGRequest(String asgName, int minSize, int desiredCapacity, int maxSize) {
		this.asgName = asgName;
		this.minSize = minSize;
		this.desiredCapacity = desiredCapacity;
		this.maxSize = maxSize;
	}

	public String getAsgName() {
		return asgName;
	}

	public void setAsgName(String asgName) {
		this.asgName = asgName;
	}

	public int getMinSize() {
		return minSize;
	}

	public void setMinSize(int minSize) {
		this.minSize = minSize;
	}

	public int getDesiredCapacity() {
		return desiredCapacity;
	}

	public void setDesiredCapacity(int desiredCapacity) {
		this.desiredCapacity = desiredCapacity;
	}

	public int getMaxSize() {
		return maxSize;
	}

	public void setMaxSize(int maxSize) {
		this.maxSize = maxSize;
	}

	@Override
	public String toString() {
		return JSONUtils.toJSON(this);
	}

}
