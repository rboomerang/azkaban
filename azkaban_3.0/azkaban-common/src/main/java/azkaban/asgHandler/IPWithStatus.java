package azkaban.asgHandler;

import azkaban.utils.JSONUtils;

public class IPWithStatus {
	private String ip;
	private int statusCode;
	private String statusMessage;
	private String instanceId;

	public IPWithStatus(String insId, String ip, int statusCode, String statusMessage) {
		this.ip = ip;
		this.instanceId = insId;
		this.statusCode = statusCode;
		this.statusMessage = statusMessage;
	}

	public IPWithStatus(String instanceId, String ip) {
		this(instanceId, ip, 200, "Service is running fine");
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public String getInstanceId() {
		return instanceId;
	}

	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}

	@Override
	public String toString() {
		return JSONUtils.toJSON(this);
	}

}
