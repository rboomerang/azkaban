#!/bin/bash


set -e
set -x

until `mysql -hdevmysql -uazkaban -pazkaban -e 'exit'`; do
  echo "Mysql is starting up.. sleeping 5 sec"
  sleep 5
done
#For db migrations lets sleep for a while
sleep 30;